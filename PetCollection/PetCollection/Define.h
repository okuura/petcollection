//
//  Define.h
//  EvEntry
//
//  Created by seo hideki on 2014/04/26.
//  Copyright (c) 2014年 seo hideki. All rights reserved.
//

#ifndef EvEntry_Define_h
#define EvEntry_Define_h

#define DEVELOPER

// 色関係
//#define seoGreenColer [UIColor colorWithRed:0.180 green:0.800 blue:0.451 alpha:1]
#define seoGreenColer [UIColor colorWithRed:190.0/255 green:219.0/255 blue:44.0/255 alpha:1]
#define seoOrangeColer [UIColor colorWithRed:0.984 green:0.800 blue:0.451 alpha:1]
#define DARK_THEME_COLOR [UIColor colorWithRed:0.66 green:0.75 blue:0.18 alpha:1.0]
#define DARK_THEME_COLOR_2 [UIColor colorWithRed:0.63 green:0.72 blue:0.17 alpha:1.0]

#define RED_TAB_COLOR [UIColor colorWithRed:0.95 green:0.42 blue:0.38 alpha:1.0] //[UIColor colorWithRed:0.95 green:0.63 blue:0.53 alpha:1.0]
#define YELLOW_TAB_COLOR [UIColor colorWithRed:1.00 green:0.67 blue:0.31 alpha:1.0] //[UIColor colorWithRed:0.95 green:0.78 blue:0.53 alpha:1.0]
#define BLUE_TAB_COLOR [UIColor colorWithRed:0.34 green:0.68 blue:0.80 alpha:1.0]  //[UIColor colorWithRed:0.47 green:0.63 blue:0.79 alpha:1.0]
#define GREEN_TAB_COLOR [UIColor colorWithRed:0.36 green:0.83 blue:0.44 alpha:1.0] //[UIColor colorWithRed:0.45 green:0.81 blue:0.63 alpha:1.0]

#define LIGHT_RED_TAB_COLOR [UIColor colorWithRed:1.00 green:0.55 blue:0.51 alpha:1.0] // 242, 161, 135, #f2a187
#define LIGHT_YELLOW_TAB_COLOR [UIColor colorWithRed:1.00 green:0.79 blue:0.37 alpha:1.0] // 242, 199, 135, #f2c787
#define LIGHT_BLUE_TAB_COLOR [UIColor colorWithRed:0.47 green:0.77 blue:0.89 alpha:1.0] // 120, 161, 201, #78a1c9
#define LIGHT_GREEN_TAB_COLOR [UIColor colorWithRed:0.47 green:0.93 blue:0.55 alpha:1.0] // 115, 207, 161, #73cfa1

#define SELECTED_ORANGE_TAB_COLOR [UIColor colorWithRed:0.94 green:0.59 blue:0.08 alpha:1.0]
#define UNSELECTED_ORANGE_TAB_COLOR [UIColor colorWithRed:0.97 green:0.84 blue:0.64 alpha:1.0]
#define ORANGE_BASE_COLOR [UIColor colorWithRed:0.97 green:0.89 blue:0.78 alpha:1.0]

#define SELECTED_PURPLE_TAB_COLOR [UIColor colorWithRed:0.42 green:0.34 blue:0.62 alpha:1.0]
#define UNSELECTED_PURPLE_TAB_COLOR [UIColor colorWithRed:0.78 green:0.76 blue:0.84 alpha:1.0]
#define PURPLE_BASE_COLOR [UIColor colorWithRed:0.83 green:0.80 blue:0.91 alpha:1.0]

#define BACKGROUND_VIEW_COLOR [UIColor colorWithRed:239.0/255 green:239.0/255 blue:244.0/255 alpha:1.0] //[UIColor colorWithRed:0.89 green:0.89 blue:0.91 alpha:1.0];
#define TABLEVIEW_SEPARATOR_COLOR [UIColor colorWithRed:224.0/255 green:224.0/255 blue:224.0/255 alpha:1.0]

/*
 *DBカラム名
 */

//店舗イベント
#define CLAM_SHOP_EVENT_DESCRIPTION @"description_"
#define CLAM_SHOP_EVENT_END @"end"
#define CLAM_SHOP_EVENT_START @"start"
#define CLAM_SHOP_EVENT_CALENDAR_ID @"calendarId"
#define CLAM_SHOP_EVENT_EVENT_ID @"eventId"
#define CLAM_SHOP_EVENT_SHOP_ID @"shopId"
#define CLAM_SHOP_EVENT_LOCATION @"location"
#define CLAM_SHOP_EVENT_SUMMARY @"summary"
#define CLAM_SHOP_EVENT_URL @"url"
#define CLAM_SHOP_EVENT_TEL @"tel"
#define CLAM_SHOP_EVENT_IMAGE @"image"
#define CLAM_SHOP_EVENT_CREATED @"created_at"
#define CLAM_SHOP_EVENT_UPDATED @"update_at"
//カレンダー情報
#define CLAM_CALENDAR_EVENT_DESCRIPTION @"description_"
#define CLAM_CALENDAR_EVENT_START @"start"
#define CLAM_CALENDAR_EVENT_ID @"id"
#define CLAM_CALENDAR_EVENT_SUMMARY @"summary"
#define CLAM_CALENDAR_EVENT_LOCATION @"location"
#define CLAM_CALENDAR_EVENT_COLOR_ID @"colorId"
#define CLAM_CALENDAR_EVENT_ALLDAY @"allDay"
#define CLAM_CALENDAR_EVENT_END @"end"
#define CLAM_CALENDAR_EVENT_CATEGORY @"category"
#define CLAM_CALENDAR_EVENT_TYPE @"type"
#define CLAM_CALENDAR_EVENT_NOTIFICATION @"notification"
//来店履歴
#define CLAM_VISIT_HISTORY_ID @"id"
#define CLAM_VISIT_HISTORY_VISIT @"visit"
//店舗情報
#define CLAM_SHOP_DETAIL_DATA_DESCRIPTION @"description_"
#define CLAM_SHOP_DETAIL_DATA_END @"end"
#define CLAM_SHOP_DETAIL_DATA_START @"start"
#define CLAM_SHOP_DETAIL_FAVORITE @"favorite"
#define CLAM_SHOP_DETAIL_DATA_HOLIDAY @"holiday"
#define CLAM_SHOP_DETAIL_DATA_SHOP_ID @"id"
#define CLAM_SHOP_DETAIL_DATA_LOCATION @"location"
#define CLAM_SHOP_DETAIL_DATA_SUMMARY @"summary"
#define CLAM_SHOP_DETAIL_DATA_URL @"url"
#define CLAM_SHOP_DETAIL_DATA_TEL @"tel"
#define CLAM_SHOP_DETAIL_DATA_IMAGE @"image"
#define CLAM_SHOP_DETAIL_DATA_CREATED @"created_at"
#define CLAM_SHOP_DETAIL_DATA_UPDATED @"update_at"
/*
 * json関連
 */
//店舗イベント
#define JSON_SHOP_EVENT_DESCRIPTION @"description"
#define JSON_SHOP_EVENT_END @"end"
#define JSON_SHOP_EVENT_START @"start"
#define JSON_SHOP_EVENT_EVENT_ID @"event_id"
#define JSON_SHOP_EVENT_SHOP_ID @"shop_id"
#define JSON_SHOP_EVENT_SHOP_NAME @"shop_name"
#define JSON_SHOP_EVENT_LOCATION @"location"
#define JSON_SHOP_EVENT_SUMMARY @"summary"
#define JSON_SHOP_EVENT_URL @"url"
#define JSON_SHOP_EVENT_TEL @"tel"
#define JSON_SHOP_EVENT_EMAIL @"email"
#define JSON_SHOP_EVENT_IMAGE @"image"
#define JSON_SHOP_EVENT_CREATED @"created_at"
#define JSON_SHOP_EVENT_UPDATED @"update_at"
//店舗情報
#define JSON_SHOP_DETAIL_DATA_DESCRIPTION @"detail"
#define JSON_SHOP_DETAIL_DATA_END @"close_time"
#define JSON_SHOP_DETAIL_DATA_START @"open_time"
#define JSON_SHOP_DETAIL_DATA_HOLIDAY @"holiday"
#define JSON_SHOP_DETAIL_DATA_SHOP_ID @"shop_id"
#define JSON_SHOP_DETAIL_DATA_LOCATION @"address"
#define JSON_SHOP_DETAIL_DATA_SUMMARY @"name"
#define JSON_SHOP_DETAIL_DATA_URL @"url"
#define JSON_SHOP_DETAIL_DATA_TEL @"phone"
#define JSON_SHOP_DETAIL_DATA_IMAGE @"image"
#define JSON_SHOP_DETAIL_DATA_CREATED @"created_at"
#define JSON_SHOP_DETAIL_DATA_UPDATED @"update_at"
//カレンダーイベント
#define JSON_CALENDAR_EVENT_DESCRIPTION @"description_"
#define JSON_CALENDAR_EVENT_START @"start"
#define JSON_CALENDAR_EVENT_ID @"id"
#define JSON_CALENDAR_EVENT_SUMMARY @"summary"
#define JSON_CALENDAR_EVENT_LOCATION @"location"
#define JSON_CALENDAR_EVENT_COLOR_ID @"colorId"
#define JSON_CALENDAR_EVENT_ALLDAY @"allDay"
#define JSON_CALENDAR_EVENT_END @"end"
#define JSON_CALENDAR_EVENT_CATEGORY @"category"
#define JSON_CALENDAR_EVENT_TYPE @"type"
#define JSON_CALENDAR_EVENT_NOTIFICATION @"notification"


/**
 * FONT関連
 */
#define DEFAULTS_JAPANESE_FONT @"HiraKakuProN-W3"
#define DEFAULTS_ENGLISH_FONT @"HelveticaNeue"
#define seoFont @"HiraKakuProN-W3"

/*
 *便利系マクロ
 *
 */
#define APP_DELEGATE ((AppDelegate*)[[UIApplication sharedApplication] delegate])
#define BUNDLE  [NSBundle bundleForClass:[self class]]
#define SCREEN_BOUNDS   ([UIScreen mainScreen].bounds)
#define LOG(A, ...) NSLog(@"DEBUG: %s:%d:%@", __PRETTY_FUNCTION__,__LINE__,[NSString stringWithFormat:A, ## __VA_ARGS__])
#define RGB(r, g, b) [UIColor colorWithRed:(r)/255.0 green:(g)/255.0 blue:(b)/255.0 alpha:1]
#define RGBA(r, g, b, a)    [UIColor colorWithRed:(r)/255.0 green:(g)/255.0 blue:(b)/255.0 alpha:(a)]
#define STATUSBAR_H 20
#define TABBAR_H    49
#define NAVBAR_H    44
#define TOOLBAR_H   44

/**
 Notification
 **/
// 文字列
#define NOTHING @"TIME_NOTHING"
#define NOTIME @"TIME_NOTIME"
#define MIN5 @"TIME_5MIN"
#define MIN15 @"TIME_15MIN"
#define MIN30 @"TIME_30MIN"
#define HOUR1 @"TIME_1HOUR"
#define HOUR2 @"TIME_2HOUR"
#define DAY1 @"TIME_1DAY"
#define DAY2 @"TIME_2DAY"
#define WEEK1 @"TIME_1WEEK"

// 時間
#define TIME_NOTHING -1
#define TIME_NOTIME 0
#define TIME_5MIN 5
#define TIME_15MIN 15
#define TIME_30MIN 30
#define TIME_1HOUR 60
#define TIME_2HOUR 120
#define TIME_1DAY 1440
#define TIME_2DAY 2880
#define TIME_1WEEK 10080



#endif
