//
//  Article1x3CustomCell.h
//  EvEntry
//
//  Created by OKUURAWataru on 2014/01/29.
//  Copyright (c) 2014年 PBL2. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "AsyncImageView.h"

@interface Article1x3CustomCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIButton *leftHugeEventButton;
@property (weak, nonatomic) IBOutlet UIButton *rightTopEventButton;
@property (weak, nonatomic) IBOutlet UIButton *rightMiddleEventButton;
@property (weak, nonatomic) IBOutlet UIButton *rightBottomEventButton;

@property (weak, nonatomic) IBOutlet UIView *leftHugeEventBackgroundView;
@property (weak, nonatomic) IBOutlet UIView *rightTopEventBackgroundView;
@property (weak, nonatomic) IBOutlet UIView *rightMiddleEventBackgroundView;
@property (weak, nonatomic) IBOutlet UIView *rightBottomEventBackgroundView;

@property (weak, nonatomic) IBOutlet UIImageView *leftHugeEventImage;
@property (weak, nonatomic) IBOutlet UIImageView *rightTopEventImage;
@property (weak, nonatomic) IBOutlet UIImageView *rightMiddleEventImage;
@property (weak, nonatomic) IBOutlet UIImageView *rightBottomEventImage;

@property (weak, nonatomic) IBOutlet UILabel *leftHugeEventTitleLabel;
@property (weak, nonatomic) IBOutlet UILabel *rightTopEventTitleLabel;
@property (weak, nonatomic) IBOutlet UILabel *rightMiddleEventTitleLabel;
@property (weak, nonatomic) IBOutlet UILabel *rightBottomEventTitleLabel;

@property (weak, nonatomic) IBOutlet UILabel *leftHugeEventHoursLabel;
@property (weak, nonatomic) IBOutlet UILabel *rightTopEventHoursLabel;
@property (weak, nonatomic) IBOutlet UILabel *rightMiddleEventHoursLabel;
@property (weak, nonatomic) IBOutlet UILabel *rightBottomEventHoursLabel;

@property (weak, nonatomic) IBOutlet UILabel *leftHugeEventShopNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *rightTopEventShopNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *rightMiddleEventShopNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *rightBottomEventShopNameLabel;

@property (weak, nonatomic) IBOutlet UILabel *leftHugeEventSubscriptLabel;
@property (weak, nonatomic) IBOutlet UILabel *rightTopEventSubscriptLabel;
@property (weak, nonatomic) IBOutlet UILabel *rightMiddleEventSubscriptLabel;
@property (weak, nonatomic) IBOutlet UILabel *rightBottomEventSubscriptLabel;



@end
