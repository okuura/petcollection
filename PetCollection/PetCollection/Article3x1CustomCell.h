//
//  Article3x1CustomCell.h
//  EvEntry
//
//  Created by OKUURAWataru on 2014/01/29.
//  Copyright (c) 2014年 PBL2. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface Article3x1CustomCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIButton *leftTopEventButton;
@property (weak, nonatomic) IBOutlet UIButton *leftMiddleEventButton;
@property (weak, nonatomic) IBOutlet UIButton *leftBottomEventButton;
@property (weak, nonatomic) IBOutlet UIButton *rightHugeEventButton;

@property (weak, nonatomic) IBOutlet UIView *leftTopEventBackgroundView;
@property (weak, nonatomic) IBOutlet UIView *leftMiddleEventBackgroundView;
@property (weak, nonatomic) IBOutlet UIView *leftBottomEventBackgroundView;
@property (weak, nonatomic) IBOutlet UIView *rightHugeEventBackgroundView;

@property (weak, nonatomic) IBOutlet UIImageView *leftTopEventImage;
@property (weak, nonatomic) IBOutlet UIImageView *leftMiddleEventImage;
@property (weak, nonatomic) IBOutlet UIImageView *leftBottomEventImage;
@property (weak, nonatomic) IBOutlet UIImageView *rightHugeEventImage;

@property (weak, nonatomic) IBOutlet UILabel *leftTopEventTitleLabel;
@property (weak, nonatomic) IBOutlet UILabel *leftMiddleEventTitleLabel;
@property (weak, nonatomic) IBOutlet UILabel *leftBottomEventTitleLabel;
@property (weak, nonatomic) IBOutlet UILabel *rightHugeEventTitleLabel;

@property (weak, nonatomic) IBOutlet UILabel *leftTopEventHoursLabel;
@property (weak, nonatomic) IBOutlet UILabel *leftMiddleEventHoursLabel;
@property (weak, nonatomic) IBOutlet UILabel *leftBottomEventHoursLabel;
@property (weak, nonatomic) IBOutlet UILabel *rightHugeEventHoursLabel;

@property (weak, nonatomic) IBOutlet UILabel *leftTopEventShopNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *leftMiddleEventShopNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *leftBottomEventShopNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *rightHugeEventShopNameLabel;

@property (weak, nonatomic) IBOutlet UILabel *leftTopEventSubscriptLabel;
@property (weak, nonatomic) IBOutlet UILabel *leftMiddleEventSubscriptLabel;
@property (weak, nonatomic) IBOutlet UILabel *leftBottomEventSubscriptLabel;
@property (weak, nonatomic) IBOutlet UILabel *rightHugeEventSubscriptLabel;



@end
