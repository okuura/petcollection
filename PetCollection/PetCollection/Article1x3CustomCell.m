//
//  Article1x3CustomCell.m
//  EvEntry
//
//  Created by OKUURAWataru on 2014/01/29.
//  Copyright (c) 2014年 PBL2. All rights reserved.
//

#import "Article1x3CustomCell.h"

@implementation Article1x3CustomCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
