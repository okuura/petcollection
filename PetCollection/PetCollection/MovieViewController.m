//
//  MovieViewController.m
//  PetCollection
//
//  Created by OKUURAWataru on 2014/10/26.
//  Copyright (c) 2014年 okuura. All rights reserved.
//

#import "MovieViewController.h"

@interface MovieViewController () {
    MPMoviePlayerController* player_;
}

@property (weak, nonatomic) IBOutlet UIView *overView;
@property UIButton *button;
- (IBAction)swipeUp:(id)sender;


@end

@implementation MovieViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // ボタンの実装
    _button = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    _button.frame = CGRectMake(10, 20, 60, 40);
    _button.layer.cornerRadius = 4;
    [_button setTitle:@"閉じる" forState:UIControlStateNormal];
    [_button setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [_button setBackgroundColor:[UIColor grayColor]];  // test [UIColor colorWithRed:0.949 green:0.929 blue:0.816 alpha:1.0]
    // ボタンがタッチダウンされた時にメソッドを呼び出す
    [_button addTarget:self action:@selector(backToMain:) forControlEvents:UIControlEventTouchDown];

    //デバイス取得
    AVCaptureDevice* device = [AVCaptureDevice defaultDeviceWithMediaType:AVMediaTypeVideo];
    NSLog(@"device:%@", device);
    //入力作成
    AVCaptureDeviceInput* deviceInput = [AVCaptureDeviceInput deviceInputWithDevice:device error:NULL];
    
    //ビデオデータ出力作成
    NSDictionary* settings = @{(id)kCVPixelBufferPixelFormatTypeKey:[NSNumber numberWithInt:kCVPixelFormatType_32BGRA]};
    AVCaptureVideoDataOutput* dataOutput = [[AVCaptureVideoDataOutput alloc] init];
    dataOutput.videoSettings = settings;
    [dataOutput setSampleBufferDelegate:self queue:dispatch_get_main_queue()];
    
    //セッション作成
    self.session = [[AVCaptureSession alloc] init];
    [self.session addInput:deviceInput];
    [self.session addOutput:dataOutput];
    self.session.sessionPreset = AVCaptureSessionPresetHigh;
    
    AVCaptureConnection *videoConnection = NULL;
    
    // カメラの向きなどを設定する
    [self.session beginConfiguration];
    
    for ( AVCaptureConnection *connection in [dataOutput connections] )
    {
        for ( AVCaptureInputPort *port in [connection inputPorts] )
        {
            if ( [[port mediaType] isEqual:AVMediaTypeVideo] )
            {
                videoConnection = connection;
                
            }
        }
    }
    if([videoConnection isVideoOrientationSupported]) // **Here it is, its always false**
    {
        [videoConnection setVideoOrientation:AVCaptureVideoOrientationPortrait];
    }
    
    [self.session commitConfiguration];
    // セッションをスタートする
    [self.session startRunning];
    
    //ムービープレイヤーの生成
    player_=[self makeMoviePlayer:@"catMovie.mp4"];
    CGRect rect = [[UIScreen mainScreen] bounds];
    [player_.view setFrame:rect];
    //ムービー完了の通知
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(moviePlayBackDidFinish:)
                                                 name:MPMoviePlayerPlaybackDidFinishNotification object:player_];
    [player_.view addSubview:_button];
    [self.overView addSubview:player_.view];
    //ムービーの再生
    [player_ play];
}

- (IBAction)movieDismissButtonTouched:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}


- (IBAction)swipeUp:(id)sender {
    NSLog(@"SwipeUp");
    
    CGPoint location = self.overView.center;
    
    if (self.overView.center.y == self.view.center.y) {
        location.y -= 1132/2;
    }
    
    [UIView animateWithDuration:0.5 animations: ^{ self.overView.center = location; }];
    
    //[player_ stop];
//    _disableButton.enabled = YES;
    
    [self startCameraControllerFromViewController:self usingDelegate:self];
}


- (IBAction)pushButton:(UIButton *)sender {
    NSLog(@"%@", [sender currentTitle]);
}


//delegateメソッド。各フレームにおける処理
- (void)captureOutput:(AVCaptureOutput *)captureOutput didOutputSampleBuffer:(CMSampleBufferRef)sampleBuffer fromConnection:(AVCaptureConnection *)connection
{
    // 画像の表示
    self.imageView.image = [self imageFromSampleBufferRef:sampleBuffer];
}

// CMSampleBufferRefをUIImageへ
- (UIImage *)imageFromSampleBufferRef:(CMSampleBufferRef)sampleBuffer
{
    // イメージバッファの取得
    CVImageBufferRef    buffer;
    buffer = CMSampleBufferGetImageBuffer(sampleBuffer);
    
    // イメージバッファのロック
    CVPixelBufferLockBaseAddress(buffer, 0);
    // イメージバッファ情報の取得
    uint8_t*    base;
    size_t      width, height, bytesPerRow;
    base = CVPixelBufferGetBaseAddress(buffer);
    width = CVPixelBufferGetWidth(buffer);
    height = CVPixelBufferGetHeight(buffer);
    bytesPerRow = CVPixelBufferGetBytesPerRow(buffer);
    
    // ビットマップコンテキストの作成
    CGColorSpaceRef colorSpace;
    CGContextRef    cgContext;
    colorSpace = CGColorSpaceCreateDeviceRGB();
    cgContext = CGBitmapContextCreate(
                                      base, width, height, 8, bytesPerRow, colorSpace,
                                      kCGBitmapByteOrder32Little | kCGImageAlphaPremultipliedFirst);
    CGColorSpaceRelease(colorSpace);
    
    // 画像の作成
    CGImageRef  cgImage;
    UIImage*    image;
    cgImage = CGBitmapContextCreateImage(cgContext);
    image = [UIImage imageWithCGImage:cgImage scale:1.0f
                          orientation:UIImageOrientationUp];
    CGImageRelease(cgImage);
    CGContextRelease(cgContext);
    
    // イメージバッファのアンロック
    CVPixelBufferUnlockBaseAddress(buffer, 0);
    return image;
}

- (MPMoviePlayerController*)makeMoviePlayer:(NSString*)res {
    //リソースのURLの生成
    //    NSString* path=[[NSBundle mainBundle] pathForResource:res ofType:@""];
    //    NSURL *_url = [NSURL fileURLWithPath:
    //                  [[NSBundle mainBundle]pathForResource:@"neko1" ofType:@"mp4"]];
    //
    
    //リソースのURLの生成
    NSString* path=[[NSBundle mainBundle] pathForResource:res ofType:@""];
    NSLog(@"path:%@", path);
    
    NSURL* _url = [NSURL fileURLWithPath:path isDirectory:false];
    
    //    NSURL* _url = [NSURL fileURLWithPath:@"/Users/guest/Desktop/petcollection/PetCollection/neko1.mp4" isDirectory:false];
    
    NSLog(@"_url:%@", _url);
    
    //ムービープレイヤーの生成
    MPMoviePlayerController* player=[[MPMoviePlayerController alloc]
                                     initWithContentURL:_url];
    player.scalingMode =MPMovieScalingModeAspectFit;
    // コントローラーを非表示
    player.controlStyle = MPMovieControlStyleNone;
    // コントローラを表示する場合は下記
    player.controlStyle=MPMovieControlStyleEmbedded;
    
    return player;
}
/**
 * ムービー完了時に呼ばれる
 */
- (void) moviePlayBackDidFinish:(NSNotification*)notification {
    //ムービー完了原因の取得
    NSDictionary* userInfo=[notification userInfo];
    int reason=[[userInfo objectForKey:
                 @"MPMoviePlayerPlaybackDidFinishReasonUserInfoKey"] intValue];
    if (reason==MPMovieFinishReasonPlaybackEnded) {
        NSLog(@"再生終了");
    } else if (reason==MPMovieFinishReasonPlaybackError) {
        NSLog(@"エラー");
    } else if (reason==MPMovieFinishReasonUserExited) {
        NSLog(@"フルスクリーン用UIのDoneボタンで終了");
    }
}

/**
 * RecordVideo
 **/
- (BOOL)startCameraControllerFromViewController:(UIViewController*)controller
                                  usingDelegate:(id )delegate {
    // 1 - Validattions
    if (([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera] == NO)
        || (delegate == nil)
        || (controller == nil)) {
        return NO;
    }
    // 2 - Get image picker
    UIImagePickerController *cameraUI = [[UIImagePickerController alloc] init];
    cameraUI.sourceType = UIImagePickerControllerSourceTypeCamera;
    // Displays a control that allows the user to choose movie capture
    cameraUI.mediaTypes = [[NSArray alloc] initWithObjects:(NSString *)kUTTypeMovie, nil];
    // Hides the controls for moving & scaling pictures, or for
    // trimming movies. To instead show the controls, use YES.
    cameraUI.allowsEditing = NO;
    cameraUI.delegate = delegate;
    // 3 - Display image picker
    [controller presentModalViewController: cameraUI animated: YES];
    return YES;
}

-(void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    NSString *mediaType = [info objectForKey: UIImagePickerControllerMediaType];
    [self dismissModalViewControllerAnimated:NO];
    // Handle a movie capture
    if (CFStringCompare ((__bridge_retained CFStringRef) mediaType, kUTTypeMovie, 0) == kCFCompareEqualTo) {
        NSString *moviePath = [[info objectForKey:UIImagePickerControllerMediaURL] path];
        if (UIVideoAtPathIsCompatibleWithSavedPhotosAlbum(moviePath)) {
            UISaveVideoAtPathToSavedPhotosAlbum(moviePath, self,
                                                @selector(video:didFinishSavingWithError:contextInfo:), nil);
        }
    }
}

-(void)video:(NSString*)videoPath didFinishSavingWithError:(NSError*)error contextInfo:(void*)contextInfo {
    if (error) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Video Saving Failed"
                                                       delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alert show];
    } else {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Video Saved" message:@"Saved To Photo Album"
                                                       delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alert show];
    }
}

-(void)backToMain:(UIButton*)button{
    NSLog(@"hoge_test");
    [self dismissViewControllerAnimated:YES completion:nil];
}



@end
