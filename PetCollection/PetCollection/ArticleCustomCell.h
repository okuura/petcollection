//
//  ArticleCustomCell.h
//  EvEntry
//
//  Created by OKUURAWataru on 2014/01/28.
//  Copyright (c) 2014年 PBL2. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ArticleCustomCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIButton *leftEventButton;
@property (weak, nonatomic) IBOutlet UIButton *rightEventButton;

@property (weak, nonatomic) IBOutlet UIView *leftEventBackgroundView;
@property (weak, nonatomic) IBOutlet UIView *rightEventBackgroundView;

@property (weak, nonatomic) IBOutlet UIImageView *leftEventImage;
@property (weak, nonatomic) IBOutlet UIImageView *rightEventImage;

@property (weak, nonatomic) IBOutlet UILabel *leftEventTitleLabel;
@property (weak, nonatomic) IBOutlet UILabel *rightEventTitleLabel;

@property (weak, nonatomic) IBOutlet UILabel *leftEventHoursLabel;
@property (weak, nonatomic) IBOutlet UILabel *rightEventHoursLabel;

@property (weak, nonatomic) IBOutlet UILabel *leftEventShopNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *rightEventShopNameLabel;

@property (weak, nonatomic) IBOutlet UILabel *leftEventSubscriptLabel;
@property (weak, nonatomic) IBOutlet UILabel *rightEventSubscriptLabel;

@end
