//
//  SecondViewController.h
//  PetCollection
//
//  Created by OKUURAWataru on 2014/10/23.
//  Copyright (c) 2014年 okuura. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AVFoundation/AVFoundation.h>

@interface SecondViewController : UIViewController <AVCaptureAudioDataOutputSampleBufferDelegate>

@property (nonatomic, strong) AVCaptureSession *session;
@property (nonatomic, strong) IBOutlet UIImageView *imageView;

@end

