//
//  ViewController.m
//  swipeTest
//
//  Created by FUJITA YASUO on 2014/07/13.
//  Copyright (c) 2014年 FUJITA YASUO. All rights reserved.
//

#import "MoviePlayViewController.h"

@interface MoviePlayViewController () {
    MPMoviePlayerController* player_;
}

@property (weak, nonatomic) IBOutlet UIView *overView;
- (IBAction)swipeUp:(id)sender;

- (IBAction)swipeRight:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *disableButton;

@end

@implementation MoviePlayViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
    
    //デバイス取得
    AVCaptureDevice* device = [AVCaptureDevice defaultDeviceWithMediaType:AVMediaTypeVideo];
    NSLog(@"device:%@", device);
    //入力作成
    AVCaptureDeviceInput* deviceInput = [AVCaptureDeviceInput deviceInputWithDevice:device error:NULL];
    
    //ビデオデータ出力作成
    NSDictionary* settings = @{(id)kCVPixelBufferPixelFormatTypeKey:[NSNumber numberWithInt:kCVPixelFormatType_32BGRA]};
    AVCaptureVideoDataOutput* dataOutput = [[AVCaptureVideoDataOutput alloc] init];
    dataOutput.videoSettings = settings;
    [dataOutput setSampleBufferDelegate:self queue:dispatch_get_main_queue()];
    
    //セッション作成
    self.session = [[AVCaptureSession alloc] init];
    [self.session addInput:deviceInput];
    [self.session addOutput:dataOutput];
    self.session.sessionPreset = AVCaptureSessionPresetHigh;
    
    AVCaptureConnection *videoConnection = NULL;
    
    // カメラの向きなどを設定する
    [self.session beginConfiguration];
    
    for ( AVCaptureConnection *connection in [dataOutput connections] )
    {
        for ( AVCaptureInputPort *port in [connection inputPorts] )
        {
            if ( [[port mediaType] isEqual:AVMediaTypeVideo] )
            {
                videoConnection = connection;
                
            }
        }
    }
    if([videoConnection isVideoOrientationSupported]) // **Here it is, its always false**
    {
        [videoConnection setVideoOrientation:AVCaptureVideoOrientationPortrait];
    }
    
    [self.session commitConfiguration];
    // セッションをスタートする
    [self.session startRunning];
    
    //ムービープレイヤーの生成
    player_=[self makeMoviePlayer:@"neko1.mp4"];
    CGRect rect = [[UIScreen mainScreen] bounds];
    [player_.view setFrame:rect];
    //ムービー完了の通知
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(moviePlayBackDidFinish:)
                                                 name:MPMoviePlayerPlaybackDidFinishNotification object:player_];
    
    [self.overView addSubview:player_.view];
    //ムービーの再生
    [player_ play];

}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)swipeUp:(id)sender {
    NSLog(@"SwipeUp");

    CGPoint location = self.overView.center;
  
    if (self.overView.center.y == self.view.center.y) {
        location.y -= 1132/2;
    }
  
    [UIView animateWithDuration:0.5 animations: ^{ self.overView.center = location; }];
    
//    [player_ stop];
    _disableButton.enabled = YES;
}


- (IBAction)pushButton:(UIButton *)sender {
  NSLog(@"%@", [sender currentTitle]);
}


//delegateメソッド。各フレームにおける処理
- (void)captureOutput:(AVCaptureOutput *)captureOutput didOutputSampleBuffer:(CMSampleBufferRef)sampleBuffer fromConnection:(AVCaptureConnection *)connection
{
    // 画像の表示
    self.imageView.image = [self imageFromSampleBufferRef:sampleBuffer];
}

// CMSampleBufferRefをUIImageへ
- (UIImage *)imageFromSampleBufferRef:(CMSampleBufferRef)sampleBuffer
{
    // イメージバッファの取得
    CVImageBufferRef    buffer;
    buffer = CMSampleBufferGetImageBuffer(sampleBuffer);
    
    // イメージバッファのロック
    CVPixelBufferLockBaseAddress(buffer, 0);
    // イメージバッファ情報の取得
    uint8_t*    base;
    size_t      width, height, bytesPerRow;
    base = CVPixelBufferGetBaseAddress(buffer);
    width = CVPixelBufferGetWidth(buffer);
    height = CVPixelBufferGetHeight(buffer);
    bytesPerRow = CVPixelBufferGetBytesPerRow(buffer);
    
    // ビットマップコンテキストの作成
    CGColorSpaceRef colorSpace;
    CGContextRef    cgContext;
    colorSpace = CGColorSpaceCreateDeviceRGB();
    cgContext = CGBitmapContextCreate(
                                      base, width, height, 8, bytesPerRow, colorSpace,
                                      kCGBitmapByteOrder32Little | kCGImageAlphaPremultipliedFirst);
    CGColorSpaceRelease(colorSpace);
    
    // 画像の作成
    CGImageRef  cgImage;
    UIImage*    image;
    cgImage = CGBitmapContextCreateImage(cgContext);
    image = [UIImage imageWithCGImage:cgImage scale:1.0f
                          orientation:UIImageOrientationUp];
    CGImageRelease(cgImage);
    CGContextRelease(cgContext);
    
    // イメージバッファのアンロック
    CVPixelBufferUnlockBaseAddress(buffer, 0);
    return image;
}

- (MPMoviePlayerController*)makeMoviePlayer:(NSString*)res {
    //リソースのURLの生成
    //    NSString* path=[[NSBundle mainBundle] pathForResource:res ofType:@""];
    //    NSURL *_url = [NSURL fileURLWithPath:
    //                  [[NSBundle mainBundle]pathForResource:@"neko1" ofType:@"mp4"]];
    //
    
    //リソースのURLの生成
    NSString* path=[[NSBundle mainBundle] pathForResource:res ofType:@""];
    NSLog(@"path:%@", path);
    
    NSURL* _url = [NSURL fileURLWithPath:path isDirectory:false];
    
    //    NSURL* _url = [NSURL fileURLWithPath:@"/Users/guest/Desktop/petcollection/PetCollection/neko1.mp4" isDirectory:false];
    
    NSLog(@"_url:%@", _url);
    
    //ムービープレイヤーの生成
    MPMoviePlayerController* player=[[MPMoviePlayerController alloc]
                                     initWithContentURL:_url];
    player.scalingMode =MPMovieScalingModeAspectFit;
    // コントローラーを非表示
    player.controlStyle = MPMovieControlStyleNone;
    // コントローラを表示する場合は下記
    player.controlStyle=MPMovieControlStyleEmbedded;
    
    return player;
}
/**
 * ムービー完了時に呼ばれる
 */
- (void) moviePlayBackDidFinish:(NSNotification*)notification {
    //ムービー完了原因の取得
    NSDictionary* userInfo=[notification userInfo];
    int reason=[[userInfo objectForKey:
                 @"MPMoviePlayerPlaybackDidFinishReasonUserInfoKey"] intValue];
    if (reason==MPMovieFinishReasonPlaybackEnded) {
        NSLog(@"再生終了");
    } else if (reason==MPMovieFinishReasonPlaybackError) {
        NSLog(@"エラー");
    } else if (reason==MPMovieFinishReasonUserExited) {
        NSLog(@"フルスクリーン用UIのDoneボタンで終了");
    }
}

- (IBAction)swipeRight:(id)sender {
    NSLog(@"SwipeRight");
    CGPoint location = _overView.center;
    if (_overView.center.x == super.view.center.x) {
        location.x += 320;
    }
    //  _overView.center = CGPointMake(200, 200);
    [UIView animateWithDuration:0.5 animations:^{_overView.center = location; }];
    
    
}

@end
