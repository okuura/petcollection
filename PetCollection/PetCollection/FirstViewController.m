//
//  FirstViewController.m
//  PetCollection
//
//  Created by OKUURAWataru on 2014/10/23.
//  Copyright (c) 2014年 okuura. All rights reserved.
//

#import "Define.h"

#import "FirstViewController.h"
#import "MovieViewController.h"

#import "ArticleCustomCell.h"
#import "Article1x3CustomCell.h"
#import "Article3x1CustomCell.h"

#define CELL_REPEAT_COUNT 4
#define EVENT_DATE_FORMAT @"yyyy-MM-dd'T'HH:mm:ssZ";


@interface FirstViewController () {
    UIColor *lightOrange;
    UIColor *lightBlue;
    UIColor *lightGreen;
    UIColor *lightPurple;
    UIColor *bigCellColor;
    bool isFirstReading;
}

@end

@implementation FirstViewController


- (void)viewDidLoad {
    
    NSLog(@"viewDidLoad");
    
    
    // タブ（ボタン）色の定義
    lightOrange = RED_TAB_COLOR; //[UIColor colorWithRed:0.95 green:0.63 blue:0.53 alpha:1.0];
    lightBlue = YELLOW_TAB_COLOR; //[UIColor colorWithRed:0.95 green:0.78 blue:0.53 alpha:1.0];
    lightGreen = BLUE_TAB_COLOR; //[UIColor colorWithRed:0.47 green:0.63 blue:0.79 alpha:1.0];
    lightPurple = GREEN_TAB_COLOR; //[UIColor colorWithRed:0.45 green:0.81 blue:0.63 alpha:1.0];

    bigCellColor = [UIColor colorWithRed:0.96 green:1.00 blue:0.91 alpha:1.0];
    
    // タブ（ボタン）色の設定
    _hotMovieButton.backgroundColor = RED_TAB_COLOR;
    _dayMovieButton.backgroundColor = LIGHT_YELLOW_TAB_COLOR;
    _categoryButton.backgroundColor = LIGHT_BLUE_TAB_COLOR;
    _otherMovieButton.backgroundColor = LIGHT_GREEN_TAB_COLOR;

    _underButtonView.backgroundColor = RED_TAB_COLOR;
    self.tableView_.backgroundColor = BACKGROUND_VIEW_COLOR;

    // タブフォントの初期色設定
    [_hotMovieButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [_dayMovieButton setTitleColor:YELLOW_TAB_COLOR forState:UIControlStateNormal];
    [_categoryButton setTitleColor:BLUE_TAB_COLOR forState:UIControlStateNormal];
    [_otherMovieButton setTitleColor:GREEN_TAB_COLOR forState:UIControlStateNormal];

    _hotMovieButton.layer.cornerRadius = 3.0;
    _dayMovieButton.layer.cornerRadius = 3.0;
    _categoryButton.layer.cornerRadius = 3.0;
    _otherMovieButton.layer.cornerRadius = 3.0;
    
    // 今どのテーブルを表示しているかを指定
    self.currentTableIs = HotMovieTable;

    // イベント表示用カスタムセルの登録
    UINib *event1x1Nib = [UINib nibWithNibName:@"ArticleCustomCell" bundle:nil];
    UINib *event1x3Nib = [UINib nibWithNibName:@"Article1x3CustomCell" bundle:nil];
    UINib *event3x1Nib = [UINib nibWithNibName:@"Article3x1CustomCell" bundle:nil];
    [self.tableView_ registerNib:event1x1Nib forCellReuseIdentifier:@"Cell1x1"];
    [self.tableView_ registerNib:event1x3Nib forCellReuseIdentifier:@"Cell1x3"];
    [self.tableView_ registerNib:event3x1Nib forCellReuseIdentifier:@"Cell3x1"];
        
    // どのセルに何個のイベントを表示するかを管理する配列を生成
    int eventNumber = 20;//[eventData getAllEventDataSize]; // イベントの個数を取得
    int cellNumber = [self eventToCell:eventNumber]; // イベントの個数からセルの数を取得
    eventNumberManagementArray = [self setEventForCell:cellNumber eventNum:eventNumber]; // どのセルに何個のイベントを表示するかを取得
    
    [super viewDidLoad];
}


#pragma mark -
#pragma mark Table View Controller Delegates

/* 表示するセルの数を設定するメソッド */
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    NSLog(@"numberOfRowsInSection");
    
    // 描画するセル数を表示
    return [self eventToCell:20];
}


/* 1x1, 1x3, 3x1などのセルにtagなどを設定するメソッド */
/* indexPath（セル数）は上記のメソッドで得られる */
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSLog(@"cellForRowAtIndexPath");
    
    // 1x3セルの描画
    if((indexPath.row % CELL_REPEAT_COUNT) == 0)
    {
        NSLog(@"indexPath row is %ld", (long)indexPath.row);
        NSString *cellIdentifier = @"Cell1x3";
        Article1x3CustomCell *cell = [self.tableView_ dequeueReusableCellWithIdentifier:cellIdentifier];
        
        // タップ時のハイライトを無くす
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        
        // ボタン・セル判別用のタグとボタンタッチ時のイベントを設定
        cell.leftHugeEventButton.tag = 0;
        cell.rightTopEventButton.tag = 1;
        cell.rightMiddleEventButton.tag = 2;
        cell.rightBottomEventButton.tag = 3;
        
        [cell.leftHugeEventButton addTarget:self action:@selector(eventTouched:event:)forControlEvents:UIControlEventTouchUpInside];
        [cell.rightTopEventButton addTarget:self action:@selector(eventTouched:event:)forControlEvents:UIControlEventTouchUpInside];
        [cell.rightMiddleEventButton addTarget:self action:@selector(eventTouched:event:)forControlEvents:UIControlEventTouchUpInside];
        [cell.rightBottomEventButton addTarget:self action:@selector(eventTouched:event:)forControlEvents:UIControlEventTouchUpInside];
        
        // イベント情報（画像、タイトル、イベント期間など）を取得・描画
        if (isFirstReading == YES)
        {
            [self set1x3CellContents:cell setEventNum:0 currentCellNum:(int)indexPath.row];
        }
        
        [self set1x3CellContents:cell setEventNum:[[eventNumberManagementArray objectAtIndex:indexPath.row]intValue] currentCellNum:(int)indexPath.row];
        
        return cell;
    }
    
    // 1x1セルの描画
    else if((indexPath.row % CELL_REPEAT_COUNT) == 1 || (indexPath.row % CELL_REPEAT_COUNT) == 3)
    {
        NSString *cellIdentifier = @"Cell1x1";
        ArticleCustomCell *cell = [self.tableView_ dequeueReusableCellWithIdentifier:cellIdentifier];
        
        // タップ時のハイライトを無くす
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        
        // ボタン・セル判別用のタグとボタンタッチ時のイベントを設定
        cell.leftEventButton.tag = 0;
        cell.rightEventButton.tag = 1;
        [cell.leftEventButton addTarget:self action:@selector(eventTouched:event:)forControlEvents:UIControlEventTouchUpInside];
        [cell.rightEventButton addTarget:self action:@selector(eventTouched:event:)forControlEvents:UIControlEventTouchUpInside];
        
        // イベント情報（画像、タイトル、イベント期間など）を取得・描画
        [self set1x1CellContents:cell setEventNum:[[eventNumberManagementArray objectAtIndex:indexPath.row]intValue] currentCellNum:(int)indexPath.row];
        
        return cell;
    }
    
    // 3x1セルの描画
    else
    {
        NSString *cellIdentifier = @"Cell3x1";
        Article3x1CustomCell *cell = [self.tableView_ dequeueReusableCellWithIdentifier:cellIdentifier];
        
        // タップ時のハイライトを無くす
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        
        // ボタン・セル判別用のタグとボタンタッチ時のイベントを設定
        cell.leftTopEventButton.tag = 0;
        cell.leftMiddleEventButton.tag = 1;
        cell.leftBottomEventButton.tag = 2;
        cell.rightHugeEventButton.tag = 3;
        
        [cell.leftTopEventButton addTarget:self action:@selector(eventTouched:event:)forControlEvents:UIControlEventTouchUpInside];
        [cell.leftMiddleEventButton addTarget:self action:@selector(eventTouched:event:)forControlEvents:UIControlEventTouchUpInside];
        [cell.leftBottomEventButton addTarget:self action:@selector(eventTouched:event:)forControlEvents:UIControlEventTouchUpInside];
        [cell.rightHugeEventButton addTarget:self action:@selector(eventTouched:event:)forControlEvents:UIControlEventTouchUpInside];
        
        // イベント情報（画像、タイトル、イベント期間など）を取得・描画
        [self set3x1CellContents:cell setEventNum:[[eventNumberManagementArray objectAtIndex:indexPath.row]intValue] currentCellNum:(int)indexPath.row];
        
        return cell;
    }
}

// セルの高さを設定
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSLog(@"heightForRowAtIndexPath");
    
    if((indexPath.row % CELL_REPEAT_COUNT) == 0) {
        return 200;
    }
    else if((indexPath.row % CELL_REPEAT_COUNT) == 1 || (indexPath.row % CELL_REPEAT_COUNT) == 3) {
        return 67;
    }
    else {
        return 200;
    }
}


#pragma mark -
#pragma mark Cell Methods

// 押したボタンが何番目のセルかを判別するメソッド
- (int)calculateRow:(int)row calculateTag:(int)tag
{
    int cellNum = (row / CELL_REPEAT_COUNT) * 12;
    
    if((row % CELL_REPEAT_COUNT) == 0)  cellNum += 0;
    else if((row % CELL_REPEAT_COUNT) == 1)  cellNum += 4;
    else if((row % CELL_REPEAT_COUNT) == 2)  cellNum += 6;
    else  cellNum += 10;
    
    cellNum += tag;
    
    return cellNum;
}

/* indexPath（現在のセル数）からイベント情報配列の開始番号を算出するメソッド */
- (int)convertCurrentCellNumToEventStartNum:(int)currentCellNum
{
    int eventStartNum = 0;
    
    if (currentCellNum % 2 == 0) {
        eventStartNum = currentCellNum * 3;
    }
    else
    {
        eventStartNum = (currentCellNum * 3) + 1;
    }
    
    return eventStartNum;
}

/* イベント数をセル数に変換するメソッド */
- (int)eventToCell:(int)eventNum
{
    int cellsetNum = eventNum / 12;
    cellsetNum = cellsetNum * 4;
    
    int additionalCellNum = 0;
    int tempAdditionalCellNum = eventNum % 12;
    
    if (tempAdditionalCellNum == 0)  additionalCellNum = 0;
    else if(1 <= tempAdditionalCellNum && tempAdditionalCellNum <= 4)  additionalCellNum = 1;
    else if(5 <= tempAdditionalCellNum && tempAdditionalCellNum <= 6)  additionalCellNum = 2;
    else if(7 <= tempAdditionalCellNum && tempAdditionalCellNum <= 10)  additionalCellNum = 3;
    else  additionalCellNum = 4; //tempAdditionalCellNum = 11の時
    
    int totalCellNum = cellsetNum + additionalCellNum;
    NSLog(@"Cellset is %d, additionalCell is %d, totalCell is %d", cellsetNum, additionalCellNum, totalCellNum);
    
    return totalCellNum;
}


/* 各セルに何個のイベント情報を配置するのかを計算するメソッド */
- (NSMutableArray *)setEventForCell:(int)cellNum eventNum:(int)eventNum
{
    NSMutableArray *eventNumArray = [NSMutableArray array];
    
    // もしeventNumに0が与えられた場合、array[0] = 0を返す
    if (eventNum == 0)
    {
        [eventNumArray insertObject:@"0" atIndex:0];
        return eventNumArray;
    }
    
    int divideEventNum = eventNum / 6;
    for (int i = 0; i < divideEventNum; i++)
    {
        [eventNumArray insertObject:@"4" atIndex:i*2];
        [eventNumArray insertObject:@"2" atIndex:i*2+1];
    }
    
    int remainderEventNum = eventNum % 6;
    if (remainderEventNum == 0)
    {
    }
    else if (1 <= remainderEventNum && remainderEventNum <= 4)
    {
        [eventNumArray insertObject:[NSString stringWithFormat:@"%d",remainderEventNum] atIndex:cellNum-1];
    }
    else
    {
        [eventNumArray insertObject:[NSString stringWithFormat:@"4"] atIndex:cellNum-2];
        [eventNumArray insertObject:[NSString stringWithFormat:@"1"] atIndex:cellNum-1];
    }
    
    return eventNumArray;
}

/* 1x3セルにコンテンツ（店舗名や営業時間など）を配置するメソッド */
/* setEventNumではセルの中に描画するイベントの個数を指定 */
- (void)set1x3CellContents:(Article1x3CustomCell *)event1x3Cell setEventNum:(int)eventNum currentCellNum:(int)currentCellNum
{
    
    NSLog(@" ");
    NSLog(@"**********");
    NSLog(@"Cell type is 1x3");
    NSLog(@"eventNum is %d", eventNum);
    NSLog(@"currentCellNum is %d", currentCellNum);
    NSLog(@"eventStartNum is %d", [self convertCurrentCellNumToEventStartNum:currentCellNum]);
    NSLog(@"**********");
    
    // イベント情報の初期化（セルの再利用対策）
    event1x3Cell.leftHugeEventImage.image = [UIImage imageNamed:@"animal1.png"];
    event1x3Cell.leftHugeEventTitleLabel.text = @"猫、まさかの展開！";
    event1x3Cell.leftHugeEventSubscriptLabel.text = @"かわいすぎる！！一発で惚れちゃいます...!!!";
    event1x3Cell.leftHugeEventHoursLabel.text = @"30:00";
    event1x3Cell.leftHugeEventShopNameLabel.text = @"ネコ好き主婦 さん";
    event1x3Cell.leftHugeEventBackgroundView.backgroundColor = bigCellColor;
    event1x3Cell.leftHugeEventBackgroundView.layer.borderWidth = 0.7;
    event1x3Cell.leftHugeEventBackgroundView.layer.borderColor = [TABLEVIEW_SEPARATOR_COLOR CGColor];
    event1x3Cell.leftHugeEventButton.enabled = YES;
    
    event1x3Cell.rightTopEventImage.image = [UIImage imageNamed:@"animal2.png"];;
    event1x3Cell.rightTopEventTitleLabel.text = @"某チキンのバレルに入るねこさん";
    event1x3Cell.rightTopEventHoursLabel.text = @"やばい！圧倒的かわいさ！";
    event1x3Cell.rightTopEventSubscriptLabel.text = @"3:57";
    event1x3Cell.rightTopEventShopNameLabel.text = @"わたる さん";
    event1x3Cell.rightTopEventBackgroundView.backgroundColor = [UIColor whiteColor];
    event1x3Cell.rightTopEventBackgroundView.layer.borderWidth = 0.7;
    event1x3Cell.rightTopEventBackgroundView.layer.borderColor = [TABLEVIEW_SEPARATOR_COLOR CGColor];
    event1x3Cell.rightTopEventButton.enabled = YES;
    
    event1x3Cell.rightMiddleEventImage.image = [UIImage imageNamed:@"animal3.png"];
    event1x3Cell.rightMiddleEventTitleLabel.text = @"ねこ鍋に集合するねこ！";
    event1x3Cell.rightMiddleEventHoursLabel.text = @"我が家のねこ大集合！";
    event1x3Cell.rightMiddleEventSubscriptLabel.text = @"9:12";
    event1x3Cell.rightMiddleEventShopNameLabel.text = @"ゆーすけ さん";
    event1x3Cell.rightMiddleEventBackgroundView.backgroundColor = [UIColor whiteColor];
    event1x3Cell.rightMiddleEventBackgroundView.layer.borderWidth = 0.7;
    event1x3Cell.rightMiddleEventBackgroundView.layer.borderColor = [TABLEVIEW_SEPARATOR_COLOR CGColor];
    event1x3Cell.rightMiddleEventButton.enabled = YES;
    
    event1x3Cell.rightBottomEventImage.image = [UIImage imageNamed:@"animal4.png"];
    event1x3Cell.rightBottomEventTitleLabel.text = @"クソでぶねこさん";
    event1x3Cell.rightBottomEventHoursLabel.text = @"でぶ過ぎて常にだらけてます..";
    event1x3Cell.rightBottomEventSubscriptLabel.text = @"12:43";
    event1x3Cell.rightBottomEventShopNameLabel.text = @"でぶねこ さん";
    event1x3Cell.rightBottomEventBackgroundView.backgroundColor = [UIColor whiteColor];
    event1x3Cell.rightBottomEventBackgroundView.layer.borderWidth = 0.7;
    event1x3Cell.rightBottomEventBackgroundView.layer.borderColor = [TABLEVIEW_SEPARATOR_COLOR CGColor];
    event1x3Cell.rightBottomEventButton.enabled = YES;
    
    // ボタンのdisable処理
    if (eventNum == 1) {
        event1x3Cell.rightTopEventButton.enabled = NO;
        event1x3Cell.rightMiddleEventButton.enabled = NO;
        event1x3Cell.rightBottomEventButton.enabled = NO;
        event1x3Cell.rightTopEventBackgroundView.backgroundColor = BACKGROUND_VIEW_COLOR; //[self getCurrentTableColor];
        event1x3Cell.rightMiddleEventBackgroundView.backgroundColor = BACKGROUND_VIEW_COLOR; //[self getCurrentTableColor];
        event1x3Cell.rightBottomEventBackgroundView.backgroundColor = BACKGROUND_VIEW_COLOR; //[self getCurrentTableColor];
    }
    else if (eventNum == 2) {
        event1x3Cell.rightMiddleEventButton.enabled = NO;
        event1x3Cell.rightBottomEventButton.enabled = NO;
        event1x3Cell.rightMiddleEventBackgroundView.backgroundColor = BACKGROUND_VIEW_COLOR; //[self getCurrentTableColor];
        event1x3Cell.rightBottomEventBackgroundView.backgroundColor = BACKGROUND_VIEW_COLOR; //[self getCurrentTableColor];
    }
    else if (eventNum == 3) {
        event1x3Cell.rightBottomEventButton.enabled = NO;
        event1x3Cell.rightBottomEventBackgroundView.backgroundColor = BACKGROUND_VIEW_COLOR; //[self getCurrentTableColor];
    }
}


/* 1x1セルにコンテンツ（店舗名や営業時間など）を配置するメソッド */
/* setEventNumではセルの中に描画するイベントの個数を指定 */
- (void)set1x1CellContents:(ArticleCustomCell *)event1x1Cell setEventNum:(int)eventNum currentCellNum:(int)currentCellNum
{
    NSLog(@" ");
    NSLog(@"**********");
    NSLog(@"Cell type is 1x1");
    NSLog(@"eventNum is %d", eventNum);
    NSLog(@"currentCellNum is %d", currentCellNum);
    NSLog(@"eventStartNum is %d", [self convertCurrentCellNumToEventStartNum:currentCellNum]);
    NSLog(@"**********");
    
    // イベント情報の初期化（セルの再利用対策）
    event1x1Cell.leftEventImage.image = [UIImage imageNamed:@"animal5.png"];
    event1x1Cell.leftEventTitleLabel.text = @"かわいいwww";
    event1x1Cell.leftEventHoursLabel.text = @"このウォンバットかわいすぎる！";
    event1x1Cell.leftEventSubscriptLabel.text = @"2:42";
    event1x1Cell.leftEventShopNameLabel.text = @"おば さん";
    event1x1Cell.leftEventBackgroundView.backgroundColor = [UIColor whiteColor];
    event1x1Cell.leftEventBackgroundView.layer.borderWidth = 0.7;
    event1x1Cell.leftEventBackgroundView.layer.borderColor = [TABLEVIEW_SEPARATOR_COLOR CGColor];
    event1x1Cell.leftEventButton.enabled = YES;
    
    event1x1Cell.rightEventImage.image = [UIImage imageNamed:@"animal6.png"];
    event1x1Cell.rightEventTitleLabel.text = @"Doge";
    event1x1Cell.rightEventHoursLabel.text = @"Kabosu the Doge";
    event1x1Cell.rightEventSubscriptLabel.text = @"1:32";
    event1x1Cell.rightEventShopNameLabel.text = @"Kabosu さん";
    event1x1Cell.rightEventBackgroundView.backgroundColor = [UIColor whiteColor];
    event1x1Cell.rightEventBackgroundView.layer.borderWidth = 0.7;
    event1x1Cell.rightEventBackgroundView.layer.borderColor = [TABLEVIEW_SEPARATOR_COLOR CGColor];
    event1x1Cell.rightEventButton.enabled = YES;
    
    // ボタンのdisable処理
    if (eventNum == 1) {
        event1x1Cell.rightEventButton.enabled = NO;
        event1x1Cell.rightEventBackgroundView.backgroundColor = BACKGROUND_VIEW_COLOR; //[self getCurrentTableColor];
    }
}


/* 3x1セルにコンテンツ（店舗名や営業時間など）を配置するメソッド */
/* setEventNumではセルの中に描画するイベントの個数を指定 */
- (void)set3x1CellContents:(Article3x1CustomCell *)event3x1Cell setEventNum:(int)eventNum currentCellNum:(int)currentCellNum
{
    NSLog(@" ");
    NSLog(@"**********");
    NSLog(@"Cell type is 3x1");
    NSLog(@"eventNum is %d", eventNum);
    NSLog(@"currentCellNum is %d", currentCellNum);
    NSLog(@"eventStartNum is %d", [self convertCurrentCellNumToEventStartNum:currentCellNum]);
    NSLog(@"**********");
    
    // イベント情報の初期化（セルの再利用対策）
    event3x1Cell.leftTopEventImage.image = [UIImage imageNamed:@"animal7.png"];
    event3x1Cell.leftTopEventTitleLabel.text = @"おっさんwith子犬軍団";
    event3x1Cell.leftTopEventHoursLabel.text = @"疲れたおっさんが一瞬で癒されます";
    event3x1Cell.leftTopEventSubscriptLabel.text = @"5:29";
    event3x1Cell.leftTopEventShopNameLabel.text = @"イヌ好き さん";
    event3x1Cell.leftTopEventBackgroundView.backgroundColor = [UIColor whiteColor];
    event3x1Cell.leftTopEventBackgroundView.layer.borderWidth = 0.7;
    event3x1Cell.leftTopEventBackgroundView.layer.borderColor = [TABLEVIEW_SEPARATOR_COLOR CGColor];
    event3x1Cell.leftTopEventButton.enabled = YES;
    
    event3x1Cell.leftMiddleEventImage.image = [UIImage imageNamed:@"animal8.png"];
    event3x1Cell.leftMiddleEventTitleLabel.text = @"デグーの群れwwww";
    event3x1Cell.leftMiddleEventHoursLabel.text = @"かわいすぎです！！";
    event3x1Cell.leftMiddleEventSubscriptLabel.text = @"9:32";
    event3x1Cell.leftMiddleEventShopNameLabel.text = @"デグデグ さん";
    event3x1Cell.leftMiddleEventBackgroundView.backgroundColor = [UIColor whiteColor];
    event3x1Cell.leftMiddleEventBackgroundView.layer.borderWidth = 0.7;
    event3x1Cell.leftMiddleEventBackgroundView.layer.borderColor = [TABLEVIEW_SEPARATOR_COLOR CGColor];
    event3x1Cell.leftMiddleEventButton.enabled = YES;
    
    event3x1Cell.leftBottomEventImage.image = [UIImage imageNamed:@"animal9.png"];
    event3x1Cell.leftBottomEventTitleLabel.text = @"寝顔wwwwwww";
    event3x1Cell.leftBottomEventHoursLabel.text = @"変顔選手権１位www";
    event3x1Cell.leftBottomEventSubscriptLabel.text = @"7:02";
    event3x1Cell.leftBottomEventShopNameLabel.text = @"イヌ男 さん";
    event3x1Cell.leftBottomEventBackgroundView.backgroundColor = [UIColor whiteColor];
    event3x1Cell.leftBottomEventBackgroundView.layer.borderWidth = 0.7;
    event3x1Cell.leftBottomEventBackgroundView.layer.borderColor = [TABLEVIEW_SEPARATOR_COLOR CGColor];
    event3x1Cell.leftBottomEventButton.enabled = YES;
    
    event3x1Cell.rightHugeEventImage.image = [UIImage imageNamed:@"animal10.png"];
    event3x1Cell.rightHugeEventTitleLabel.text = @"回し車の使い方wwww";
    event3x1Cell.rightHugeEventSubscriptLabel.text = @"その使い方間違ってるンゴwwwwww";
    event3x1Cell.rightHugeEventHoursLabel.text = @"5:00";
    event3x1Cell.rightHugeEventShopNameLabel.text = @"ham さん";
    event3x1Cell.rightHugeEventBackgroundView.backgroundColor = bigCellColor;
    event3x1Cell.rightHugeEventBackgroundView.layer.borderWidth = 0.7;
    event3x1Cell.rightHugeEventBackgroundView.layer.borderColor = [TABLEVIEW_SEPARATOR_COLOR CGColor];
    event3x1Cell.rightHugeEventButton.enabled = YES;
    
    // ボタンのdisable処理
    if (eventNum == 1) {
        event3x1Cell.leftMiddleEventButton.enabled = NO;
        event3x1Cell.leftBottomEventButton.enabled = NO;
        event3x1Cell.rightHugeEventButton.enabled = NO;
        event3x1Cell.leftMiddleEventBackgroundView.backgroundColor = BACKGROUND_VIEW_COLOR; //[self getCurrentTableColor];
        event3x1Cell.leftBottomEventBackgroundView.backgroundColor = BACKGROUND_VIEW_COLOR; //[self getCurrentTableColor];
        event3x1Cell.rightHugeEventBackgroundView.backgroundColor = BACKGROUND_VIEW_COLOR; //[self getCurrentTableColor];
    }
    else if (eventNum == 2) {
        event3x1Cell.leftBottomEventButton.enabled = NO;
        event3x1Cell.rightHugeEventButton.enabled = NO;
        event3x1Cell.leftBottomEventBackgroundView.backgroundColor = BACKGROUND_VIEW_COLOR; //[self getCurrentTableColor];
        event3x1Cell.rightHugeEventBackgroundView.backgroundColor = BACKGROUND_VIEW_COLOR; //[self getCurrentTableColor];
    }
    else if (eventNum == 3) {
        event3x1Cell.rightHugeEventButton.enabled = NO;
        event3x1Cell.rightHugeEventBackgroundView.backgroundColor = BACKGROUND_VIEW_COLOR; //[self getCurrentTableColor];
    }
    
}

#pragma mark -
#pragma mark Button Action Contents

// ホットボタンタッチ時のイベント
- (IBAction)hotMovieButtonTouched:(id)sender
{
    NSLog(@"Hot event button touched.");
    
    _underButtonView.backgroundColor = RED_TAB_COLOR;
    
    [_hotMovieButton.titleLabel setFont:[UIFont systemFontOfSize:17]];
    [_dayMovieButton.titleLabel setFont:[UIFont systemFontOfSize:15]];
    [_categoryButton.titleLabel setFont:[UIFont systemFontOfSize:15]];
    [_otherMovieButton.titleLabel setFont:[UIFont systemFontOfSize:15]];
    
    _hotMovieButton.backgroundColor = RED_TAB_COLOR;
    _dayMovieButton.backgroundColor = LIGHT_YELLOW_TAB_COLOR;
    _categoryButton.backgroundColor = LIGHT_BLUE_TAB_COLOR;
    _otherMovieButton.backgroundColor = LIGHT_GREEN_TAB_COLOR;
    
    [_hotMovieButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [_dayMovieButton setTitleColor:YELLOW_TAB_COLOR forState:UIControlStateNormal];
    [_categoryButton setTitleColor:BLUE_TAB_COLOR forState:UIControlStateNormal];
    [_otherMovieButton setTitleColor:GREEN_TAB_COLOR forState:UIControlStateNormal];
    
    self.currentTableIs = HotMovieTable;
}


// 今日ボタンタッチ時のイベント
- (IBAction)dayMovieButtonTouched:(id)sender
{
    NSLog(@"Day event button touched.");
    
    _underButtonView.backgroundColor = YELLOW_TAB_COLOR;
    
    [_dayMovieButton.titleLabel setFont:[UIFont systemFontOfSize:17]];
    [_hotMovieButton.titleLabel setFont:[UIFont systemFontOfSize:15]];
    [_categoryButton.titleLabel setFont:[UIFont systemFontOfSize:15]];
    [_otherMovieButton.titleLabel setFont:[UIFont systemFontOfSize:15]];
    
    _hotMovieButton.backgroundColor = LIGHT_RED_TAB_COLOR;
    _dayMovieButton.backgroundColor = YELLOW_TAB_COLOR;
    _categoryButton.backgroundColor = LIGHT_BLUE_TAB_COLOR;
    _otherMovieButton.backgroundColor = LIGHT_GREEN_TAB_COLOR;
    
    [_dayMovieButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [_hotMovieButton setTitleColor:RED_TAB_COLOR forState:UIControlStateNormal];
    [_categoryButton setTitleColor:BLUE_TAB_COLOR forState:UIControlStateNormal];
    [_otherMovieButton setTitleColor:GREEN_TAB_COLOR forState:UIControlStateNormal];
    
    self.currentTableIs = DayMovieTable;
}


// 今週ボタンタッチ時のイベント
- (IBAction)categoryButtonTouched:(id)sender
{
    NSLog(@"Week event button touched.");
    
    // 今週ボタンの色に変更する
    _underButtonView.backgroundColor = BLUE_TAB_COLOR;
    
    [_categoryButton.titleLabel setFont:[UIFont systemFontOfSize:17]];
    [_dayMovieButton.titleLabel setFont:[UIFont systemFontOfSize:15]];
    [_hotMovieButton.titleLabel setFont:[UIFont systemFontOfSize:15]];
    [_otherMovieButton.titleLabel setFont:[UIFont systemFontOfSize:15]];
    
    _hotMovieButton.backgroundColor = LIGHT_RED_TAB_COLOR;
    _dayMovieButton.backgroundColor = LIGHT_YELLOW_TAB_COLOR;
    _categoryButton.backgroundColor = BLUE_TAB_COLOR;
    _otherMovieButton.backgroundColor = LIGHT_GREEN_TAB_COLOR;
    
    [_categoryButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [_hotMovieButton setTitleColor:RED_TAB_COLOR forState:UIControlStateNormal];
    [_dayMovieButton setTitleColor:YELLOW_TAB_COLOR forState:UIControlStateNormal];
    [_otherMovieButton setTitleColor:GREEN_TAB_COLOR forState:UIControlStateNormal];
    
    self.currentTableIs = CategoryTable;
}


// 今月ボタンタッチ時のイベント
- (IBAction)otherMovieButtonTouched:(id)sender
{
    NSLog(@"Month event button touched.");
    
    _underButtonView.backgroundColor = GREEN_TAB_COLOR;
    
    [_otherMovieButton.titleLabel setFont:[UIFont systemFontOfSize:17]];
    [_dayMovieButton.titleLabel setFont:[UIFont systemFontOfSize:15]];
    [_hotMovieButton.titleLabel setFont:[UIFont systemFontOfSize:15]];
    [_categoryButton.titleLabel setFont:[UIFont systemFontOfSize:15]];
    
    _hotMovieButton.backgroundColor = LIGHT_RED_TAB_COLOR;
    _dayMovieButton.backgroundColor = LIGHT_YELLOW_TAB_COLOR;
    _categoryButton.backgroundColor = LIGHT_BLUE_TAB_COLOR;
    _otherMovieButton.backgroundColor = GREEN_TAB_COLOR;
    
    [_otherMovieButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [_hotMovieButton setTitleColor:RED_TAB_COLOR forState:UIControlStateNormal];
    [_dayMovieButton setTitleColor:YELLOW_TAB_COLOR forState:UIControlStateNormal];
    [_categoryButton setTitleColor:BLUE_TAB_COLOR forState:UIControlStateNormal];
    
    self.currentTableIs = OtherMovieTable;
}

// タッチしたボタンが存在するセルが何番目かを判定
- (IBAction)eventTouched:(id)sender event:(UIEvent *) event
{
    // 押されたボタンのセルのインデックスを取得
    UITouch *touch = [[event allTouches] anyObject];
    CGPoint point = [touch locationInView:self.tableView_];
    NSIndexPath *indexPath = [self.tableView_ indexPathForRowAtPoint:point];
    
    UIButton *button = (UIButton *)sender;
    
    // 押したボタンは何番目の要素かを判別
    int cellNum = [self calculateRow:(int)indexPath.row calculateTag:(int)button.tag];
    NSLog(@"Touched cell is %d",cellNum);
    
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    MovieViewController *controller = [storyboard instantiateViewControllerWithIdentifier:@"MovieView"];
    [self presentViewController:controller animated:YES completion:nil];
}

@end
