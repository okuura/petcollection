//
//  AppDelegate.h
//  PetCollection
//
//  Created by OKUURAWataru on 2014/10/23.
//  Copyright (c) 2014年 okuura. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

