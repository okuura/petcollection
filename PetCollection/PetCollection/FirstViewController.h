//
//  FirstViewController.h
//  PetCollection
//
//  Created by OKUURAWataru on 2014/10/23.
//  Copyright (c) 2014年 okuura. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef enum
{
    HotMovieTable,
    DayMovieTable,
    CategoryTable,
    OtherMovieTable
} CurrentTable;

@interface FirstViewController : UIViewController <UITableViewDataSource, UITableViewDelegate> {
    NSMutableArray *eventNumberManagementArray;
//    NSMutableArray *eventDataDictionaryForTable;
//    NSMutableArray *eventDataDictionaryForStore;
}

@property (weak, nonatomic) IBOutlet UIButton *hotMovieButton;
@property (weak, nonatomic) IBOutlet UIButton *dayMovieButton;
@property (weak, nonatomic) IBOutlet UIButton *categoryButton;
@property (weak, nonatomic) IBOutlet UIButton *otherMovieButton;

@property (weak, nonatomic) IBOutlet UITableView *tableView_;
@property (weak, nonatomic) IBOutlet UIView *underButtonView;

@property (nonatomic, assign) CurrentTable currentTableIs;

- (IBAction)hotMovieButtonTouched:(id)sender;
- (IBAction)dayMovieButtonTouched:(id)sender;
- (IBAction)categoryButtonTouched:(id)sender;
- (IBAction)otherMovieButtonTouched:(id)sender;

@end

